﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace code_assignment.Migrations
{
    public partial class initialDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Campaigns",
                columns: table => new
                {
                    CampaignId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    CreatedDate = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AuthorId = table.Column<string>(type: "nvarchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campaigns", x => x.CampaignId);
                    table.ForeignKey(
                        name: "FK_Campaigns_Users_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Inputs",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    InputType = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    InputValue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CampaignId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Inputs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Inputs_Campaigns_CampaignId",
                        column: x => x.CampaignId,
                        principalTable: "Campaigns",
                        principalColumn: "CampaignId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Campaigns",
                columns: new[] { "CampaignId", "AuthorId", "CreatedDate" },
                values: new object[,]
                {
                    { "camp0001", null, "17/10/2021 12.10.09" },
                    { "camp0002",null, "17/10/2021 12.10.09" },
                    { "camp0003", null, "17/10/2021 12.10.09" }
                });

            migrationBuilder.InsertData(
                table: "Inputs",
                columns: new[] { "Id", "CampaignId", "InputType", "InputValue" },
                values: new object[,]
                {
                    { "Input0001", null, "campaign_name", "Street_Campaign" },
                    { "Input0002", null, "channel", "Direct_Marketing" },
                    { "Input0003", null, "source", "Survey0001" },
                    { "Input0004", null, "target_url", "www.myurl.com" },
                    { "Input0005", null, "campaign_name", "Online_Campaign" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Email", "Name" },
                values: new object[,]
                {
                    { "000U1", "user1@email.com", "User1" },
                    { "000U2", "user2@email.com", "User2" },
                    { "000U3", "user3@email.com", "User3" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_AuthorId",
                table: "Campaigns",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Inputs_CampaignId",
                table: "Inputs",
                column: "CampaignId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Inputs");

            migrationBuilder.DropTable(
                name: "Campaigns");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
