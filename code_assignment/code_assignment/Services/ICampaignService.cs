﻿using code_assignment.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace code_assignment.Services
{
    public interface ICampaignService
    {
        public Task<IEnumerable<Campaigns>> GetAllCampaignsAsync();

        public Task<IEnumerable<Users>> GetAllUsersAsync();
        public Task<Users> GetUserByEmailAsync(String Email);
        public Task<Campaigns> PostCampaignAsync(Campaigns campaign);

    }
}
