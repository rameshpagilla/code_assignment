﻿using code_assignment.Models.DBContext;
using code_assignment.Models.Domain;
using code_assignment.Models.DTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace code_assignment.Services
{
    public class CampaignService : ICampaignService
    {
        private readonly DBContext _dBContext;

        public CampaignService(DBContext dBContext)
        {
           _dBContext=dBContext;
        }

        public async Task<IEnumerable<Campaigns>> GetAllCampaignsAsync()
        {
            return await _dBContext.Campaigns
                  .Include(c=>c.Inputs)
                  .OrderByDescending(c => c.CreatedDate)
                  .ToListAsync();
        }

        public async  Task<IEnumerable<Users>> GetAllUsersAsync()
        {
            return await _dBContext.Users
        .Include(u => u.Campaigns)
         .ToListAsync();
        }

        public async Task<Users> GetUserByEmailAsync(string Email)
        {
            Users user = await _dBContext.Users
                .Where(u => u.Email == Email)
                .FirstAsync();
            return user;
        }

        public async Task<Campaigns> PostCampaignAsync(Campaigns campaign)
        {
            _dBContext.Campaigns.Add(campaign);
            await _dBContext.SaveChangesAsync();
            return campaign;
        }
    }
}
