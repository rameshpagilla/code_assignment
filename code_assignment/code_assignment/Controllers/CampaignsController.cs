﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using code_assignment.Models.DBContext;
using code_assignment.Models.Domain;
using Microsoft.AspNetCore.Routing;
using code_assignment.Services;
using AutoMapper;
using code_assignment.Models.DTOs;

namespace code_assignment.Controllers
{
    [Route("api/v1/campaigns")]
    [ApiController]

    public class CampaignsController : Controller
    {
        private readonly DBContext _context;
        private readonly ICampaignService _service;
        private readonly IMapper _mapper;

        public CampaignsController(DBContext context,ICampaignService service,IMapper mapper)
        {
            _context = context;
            _service = service;
            _mapper = mapper;
        }
        /// <summary>
        /// 
        ///View All Campaingns
        /// </summary>
        /// <returns></returns>

        // GET: Campaigns
        [HttpGet]

        public async Task<IEnumerable<Campaigns>> ViewAllCampaigns()
        {

            return await _service.GetAllCampaignsAsync();
        }
        /// <summary>
        /// View all users
        /// </summary>
        /// <returns></returns>
        [HttpGet("/users")]
        public async Task<IEnumerable<Users>> GetUsers()
        {
            return await _service.GetAllUsersAsync();

        }
        /// <summary>
        /// Get User BY Email Id
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet("/User/{email}")]
        public async Task<Users> getUserByEmail(string email)
        {
            return await _service.GetUserByEmailAsync(email);
        }
        [HttpPost]
        public async Task<Campaigns> PostCampaign(Campaigns campaign)
        {
           Campaigns domainCampaign= await _service.PostCampaignAsync(campaign);
            return domainCampaign;
        }
       
    }
}
