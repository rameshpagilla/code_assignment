﻿using AutoMapper;
using code_assignment.Models.Domain;
using code_assignment.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace code_assignment.Profiles
{
    public class usersProfile : Profile
    {
        public usersProfile()
        {
            CreateMap<Users, readUsersDto>()
                .ForMember(udto => udto.Campaigns, opt => opt
                   .MapFrom(u => u.Campaigns.Select(c => c.CampaignId).ToArray()))
                .ReverseMap();
        }
    }
}
