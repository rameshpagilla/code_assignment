﻿using AutoMapper;
using code_assignment.Models.Domain;
using code_assignment.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace code_assignment.Profiles
{
    public class CampaignProfile : Profile
    {
        public CampaignProfile()
        {
            CreateMap<Campaigns, PostCampaignDto>()
                .ForMember(cdto => cdto.AuthorId, opt => opt
                   .MapFrom(c => c.Author));
            CreateMap<Campaigns, campaignReadDto>()
                .ForMember(cdto => cdto.Author, opt => opt
                   .MapFrom(c => c.Author.Name))
                .ForMember(cdto => cdto.Inputs, opt => opt
                  .MapFrom(c => c.Inputs.Select(i => i.Id)
                  .ToArray()));
                
        }
    }
}
