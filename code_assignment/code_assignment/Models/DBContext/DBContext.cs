﻿using code_assignment.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace code_assignment.Models.DBContext
{
    public class DBContext:DbContext
    {
        public DBContext(DbContextOptions options):base(options)
        {
        }

        public DbSet<Users> Users { get; set; }
        public DbSet<Campaigns> Campaigns { get; set; }
        public DbSet<Inputs> Inputs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Inputs>()
          .Property(i => i.InputType)
          .HasConversion<string>()
          .HasMaxLength(50);

            modelBuilder.Entity<Users>().HasData(new Users { Id = "000U1", Name = "User1" ,Email="user1@email.com" });
            modelBuilder.Entity<Users>().HasData(new Users { Id = "000U2", Name = "User2", Email = "user2@email.com" });
            modelBuilder.Entity<Users>().HasData(new Users { Id = "000U3", Name = "User3",  Email = "user3@email.com" });


            modelBuilder.Entity<Campaigns>().HasData(new Campaigns { CampaignId = "camp0001",CreatedDate=DateTime.Now.ToString()});
            modelBuilder.Entity<Campaigns>().HasData(new Campaigns { CampaignId = "camp0002", CreatedDate = DateTime.Now.ToString() });
            modelBuilder.Entity<Campaigns>().HasData(new Campaigns { CampaignId = "camp0003",  CreatedDate = DateTime.Now.ToString() });

            modelBuilder.Entity<Inputs>().HasData(new Inputs { Id = "Input0001", InputType=InputType.campaign_name,InputValue="Street_Campaign"  });
            modelBuilder.Entity<Inputs>().HasData(new Inputs { Id = "Input0002", InputType = InputType.channel, InputValue = "Direct_Marketing"});
            modelBuilder.Entity<Inputs>().HasData(new Inputs { Id = "Input0003", InputType = InputType.source, InputValue = "Survey0001" });
            modelBuilder.Entity<Inputs>().HasData(new Inputs { Id = "Input0004", InputType = InputType.target_url, InputValue = "www.myurl.com"  });
            modelBuilder.Entity<Inputs>().HasData(new Inputs { Id = "Input0005", InputType = InputType.campaign_name, InputValue = "Online_Campaign" });
                
        }

    }
}
