﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace code_assignment.Models.Domain
{
    public enum InputType
    {

        channel,
        source,
        campaign_name,
        target_url
    }
    public class Inputs
    {
        //Primary_key
        [Key]
        public String Id { get; set; }
        public InputType InputType { get; set; }
        public String  InputValue { get; set; }

        //Relationships
        public Campaigns Campaign { get; set; }
    }
}
