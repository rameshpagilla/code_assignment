﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace code_assignment.Models.Domain
{
    public class Campaigns
    {
        //Primary_key
        [Key]
        public String CampaignId { get; set; }
       [Required]
        public String CreatedDate { get; set; }
        //Relationships
        public Users Author { get; set; }
        public ICollection<Inputs> Inputs { get; set; }


    }
}
