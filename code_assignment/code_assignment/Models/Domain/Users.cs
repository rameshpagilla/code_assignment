﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace code_assignment.Models.Domain
{
    public class Users
    {
        //Primary_Key
        [Key]
        [MaxLength(50)]
        public String Id { get; set; }

        [Required]
        [MaxLength(50)]
        public String Name { get; set; }

        [Required]
        [MaxLength(50)]
        public String Email { get; set; }
      
        //Relationships
        public ICollection<Campaigns> Campaigns { get; set; }

    }
}
