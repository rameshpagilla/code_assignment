﻿using code_assignment.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace code_assignment.Models.DTOs
{
    public class PostCampaignDto
    {
        public String CampaignId { get; set; }
  
        public String CreatedDate { get; set; }
        //Relationships
        public String  AuthorId { get; set; }
        public List<int> Inputs { get; set; }
    }
}
