﻿using code_assignment.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace code_assignment.Models.DTOs
{
    public class readUsersDto
    {
        public String Id { get; set; }
        public String Name { get; set; }
        public String Email { get; set; }
   //Relationships
        public List<Campaigns> Campaigns { get; set; }
    }
}
