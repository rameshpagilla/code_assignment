﻿using AutoMapper;
using code_assignment.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace code_assignment.Models.DTOs
{
    public class campaignReadDto
    {
        public String CampaignId { get; set; }
      
        public String CreatedDate { get; set; }
        //Relationships
        public String Author{ get; set; }
        public ICollection<Inputs> Inputs { get; set; }

    }
}
